extends KinematicBody2D

var initial_speed = 400
export (int) var speed = initial_speed
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)

var velocity = Vector2()

func get_input():
	velocity.x = 0
	var jump_again = false
	if is_on_floor() and Input.is_action_just_pressed('up'):
		velocity.y = jump_speed
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
		
	#for bonus : dash
	if Input.is_key_pressed(32):
		speed *= sqrt(2)
	if not Input.is_key_pressed(32):
		speed = initial_speed

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
